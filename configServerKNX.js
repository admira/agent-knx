var light = {
	"41592": {
		"name"	:	"Alea - VW Residence 1",
	    "port"	:	{ 
			"name" : "COM3",
	        "config" : {
	        	"baudRate" : 9600,
		        "dataBits" : 8,
		        "stopBits" : 1
		    }
	    }
	},
	"41612": {
		"name"	:	"Catalana - VW Residence 2", 
	    "port"	:	{ 
			"name" : "COM3",
	        "config" : {
	        	"baudRate" : 9600,
		        "dataBits" : 8,
		        "stopBits" : 1
		    }
	    }
	},
	"41620": {
		"name"	:	"Sant Just - VW Residence 1",
	    "port"	:	{ 
			"name" : "COM4",
	        "config" : {
	        	"baudRate" : 9600,
		        "dataBits" : 8,
		        "stopBits" : 1
		    }
	    }
	},
	"41633": {
		"name"	:	"Malilla - VW Residence 1",
	    "port"	:	{ 
			"name" : "COM4",
	        "config" : {
	        	"baudRate" : 9600,
		        "dataBits" : 8,
		        "stopBits" : 1
		    }
	    }
	},
	"00000": {
		"name"	:	"Marbella",
	    "port"	:	{ 
			"name" : "COM0",
	        "config" : {
	        	"baudRate" : 9600,
		        "dataBits" : 8,
		        "stopBits" : 1
		    }
	    }
	},
	"41668": {
		"name"	:	"Urduliz - VW Residence 2",
	    "port"	:	{ 
			"name" : "COM3",
	        "config" : {
	        	"baudRate" : 9600,
		        "dataBits" : 8,
		        "stopBits" : 1
		    }
	    }
	},
	"42817": {
		"name"	:	"Bolueta - VW  Residence 1",
	    "port"	:	{ 
			"name" : "COM4",
	        "config" : {
	        	"baudRate" : 9600,
		        "dataBits" : 8,
		        "stopBits" : 1
		    }
	    }
 	},

	"42831": {
		"name"	:	"Malaga - VW  Residence 2",
	    "port"	:	{ 
			"name" : "COM4",
	        "config" : {
	        	"baudRate" : 9600,
		        "dataBits" : 8,
		        "stopBits" : 1
		    }
	    }
 	}
}
