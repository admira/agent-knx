/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
console.log("Agent KNX - v1.0.1")


var SerialPort = require('serialport');
var port = false;
var fs = require('fs');
var xml2js = require('xml2js');

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

// Create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.use(express.static('public'));
app.get('/index', function (req, res) {
    console.log("index")
    response = {
        name : "agenKNX"
    };
    res.end(JSON.stringify(response));
})

app.post('/sendCommand', urlencodedParser, function (req, res) {
    // Prepare output in JSON format
    var command = req.body.command;
    var hexData   = Buffer.from ( command , 'hex' );
    console.log( 'Sending data: ' )
    console.log( hexData );

    response = {
        command : command,
        buff    : hexData,
        send    : false
    };

    if ( port ) {
        port.write( hexData );
        console.log( 'Data send ' )
        response.send = true;
    }

   console.log(response);
   res.end(JSON.stringify(response));

})

var server = app.listen(3002, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)

})



/************************************************* com ports check, launch antennas *************************************************/
// SerialPort.list(function (err, ports) {
//     ports.forEach(function(port) {
//     function ( port ) {
//         if ( port.manufacturer == 'FTDI' ){
//             launchPortInventory(port.comName);
//         }
//     }( port );
//     });
// });

/************************************************* Launcher  *************************************************/

portLoaded = false; 
coreloading ();

function coreloading() {
    var parser = new xml2js.Parser();
    fs.readFile('../../core.xml', function(err, data) {
        parser.parseString(data, function (err, result) {
            if (err) console.log ('Error loading core: '+ err)
            console.log(result.core.config[0].id[0]);
            console.log('Core loaded ');
            loading( result.core.config[0].id[0] );
        });
    });
}




function loading ( id ) {
    console.log("Load check");
    setTimeout ( function () { loading (); }, 30000 );
    if (!portLoaded) {
        readPortConfig ( id );
    }

}


function readPortConfig ( id ) {
    console.log("readPortConfig");
    try{
        eval(fs.readFileSync('../../content/configServerKNX.js')+'');
        config = light[id];
        var portName = config.port.name;
        var portConfig = config.port.config;
        portConfig.parser = SerialPort.parsers.byteLength(5)

        launchPort ( portName, portConfig )
    }catch ( error ){
        console.log(error);
        return null;
    }  
}

// function readPortConfig () {
//     console.log("readPortConfig");
//     try{
//         config = JSON.parse( fs.readFileSync( "./configKNX.json" ) );
//         var portName = config.port.name;
//         var portConfig = config.port.config;
//         portConfig.parser = SerialPort.parsers.byteLength(5)

//         launchPort ( portName, portConfig )
//     }catch ( error ){
//         console.log(error);
//         return null;
//     }  
// }


function launchPort ( portName, portConfig ) {
    console.log("launchPort");
    console.log( portName );
    console.log( portConfig );
    port = new SerialPort( portName, portConfig );


    port.on('open', function (err){
        if (err) {
            return console.log('Error opening port: ', err.message);
        }
        portLoaded = true;
        // var data   = Buffer.from ( 'D2D2FFD2D2' , 'hex' );
        // port.write( data );

        console.log ('Coms with KNX started in: ' + portName );
    });


    port.on('error', function(err) {
        console.log('Error: ', err.message);
    })


    port.on('data', function (data) {
        buff = Buffer.from(data,'hex');
        console.log( 'Recieved data: ' )
        console.log( buff );
        console.log( 'Recieved data ascii: ' )
        console.log ( buff.toString('ascii') );
        // if ( buff.slice(3,7).toString('ascii') == '6CA7'){
        //     parseInventory ( data.slice (7,-1), port );
        // }
        
    });

    port.on('close', function ( data ){
        portLoaded = false;
    })
}


/************************************************* KNX check status functions  *************************************************/














/************************************************* communication functions  *************************************************/

var STX = Buffer.from ( '02' , 'hex');
var ETX = Buffer.from ( '03' , 'hex');
var ACK = Buffer.from ( '06' , 'hex');
var NAK = Buffer.from ( '15' , 'hex');
var SYN = Buffer.from ( '16' , 'hex');
var ESC = Buffer.from ( '1B' , 'hex');



/************************************************* Admira connection *************************************************/

function sendCondition ( tag ){
    console.log ( 'Condition fired for : ' + tag );
    var now = new Date();
    tstamp = Math.floor( now.getTime() / 1000 );
    // fs.writeFile( 'C:/ADmiraPlayer/Admira/conditions/biomax.xml',
    //                                 '<?xml version="1.0" encoding="UTF-8"?><condition id="07" tstamp="' +
    //                                 tstamp + 
    //                                 '" version="1.0" status="ok" ><value>' + 
    //                                 tag + 
    //                                 '</value><info> Service OK</info></condition>',
    //             function(err){
    //                 if(err){
    //                     console.log('Writing analytics condition data error: ' + err);
    //                 }else{
    //                     console.log(tag);
    //                 }
    //             }
    // );
}

function sendStats ( tag ){
    console.log ( 'Stats fired for : ' + tag );
    var now = new Date();
    tstamp = Math.floor( now.getTime() / 1000 );
    var aux = {
         'device': 5700,
         'type': 'kimaldi',
         'tstamp': tstamp,
         'data': [ {'reader' : 01 }, {'tag' : tag }, { 'average' : 0 } ]
     }
    // fs.writeFile( 'C:/ADmiraPlayer/Admira/modules/silo/object_' + tag + '_' + tstamp + '.json',
    //                                 '<?xml version="1.0" encoding="UTF-8"?><condition id="02" tstamp="' +
    //                                 tstamp + 
    //                                 '" version="1.0" status="ok" ><value>' + 
    //                                 tag + 
    //                                 '</value><info> Service OK</info></condition>',
    //             function(err){
    //                 if(err){
    //                     console.log('Writing analytics condition data error: ' + err);
    //                 }else{
    //                     console.log(tag);
    //                 }
    //             }
    // );
}